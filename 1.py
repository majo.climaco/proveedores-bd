import pymysql
import requests
import json

class DataBase:
    def __init__(self):
        self.connection = pymysql.connect(
            host='localhost',
            user='root',
            password='triplede',
            db='triplede'
        )

        self.cursor = self.connection.cursor()
        print("Conexión establecida")
        
    def select_proveedor(self):
        sql = 'SELECT * FROM proveedores ORDER BY proveedor ASC LIMIT 1'

        try:
            self.cursor.execute(sql)
            proveedores = self.cursor.fetchall()

            for prov in proveedores:
                print("______\n")
                print("PROVEEDORES:", prov[0])
                print("NO_PARTE:", prov[1])
                print("APLICACIÓN:", prov[2])
                print("PRECIO:", prov[3])
                print("ÚLTIMO_COSTO:", prov[4])
                print("REQUEST:", prov[5])
                print("RESPONSE:", prov[6])

                api_url = "https://webservice.tecalliance.services/pegasus-3-0/services/TecdocToCatDLB.jsonEndpoint?api_key=2BeBXg6JLT9f7z2abTyuwizVNAHmEiqzbBEVpzQ5WqB5VADLvpDu"
                todo = {
                    "getArticles": {
                        "articleCountry": "MX",
                        "provider": 23193,
                        "searchQuery": prov[1],
                        "lang": "qd",
                        "includeAll": True,
                        "includeArticleText": True,
                        "includeReplacesArticles": True,
                        "includeArticleCriteria": True,
                        "includeParentArticles": True,
                        "includePartsListArticles": True,
                        "includeImages": True,
                        "includePrices": True
                }
                }
                response = requests.post(api_url, json=todo)
                todos = json.loads(response.text)

                #print(response.json())
                #jsonData = response.json()
                #jsonToPython = json.loads(jsonData)

                print("_______________\n")
                print(todos)

                val = (todos)
                val1 = (prov[1])

                print("______\n")
                print("VARIABLES")
                print("RESPONSE:", val)
                print("NO_PARTE:", val1)
                sql = "UPDATE proveedores SET response = @val WHERE no_parte = @val1"
                #sql = "UPDATE `proveedores` SET response = '%s' WHERE no_parte = '%s'"
                #sql = "UPDATE `proveedores` (`response`) VALUES (%s) WHERE (`no_parte`) VALUES (%s)"
                self.cursor.execute(sql)

                #print(jsonData)
                print("listo")

        except Exception as e:
            raise

database = DataBase()
database.select_proveedor()
